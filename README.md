# Nexus - An open-sourced script sharing platform.
Nexus is a ROBLOX script storage system to connect through various scripting utilities.

You can clone the GitLab repo to use this.

# Steps on how to use Nexus:
- Clone the GitLab repo using ``Git``.

```
git clone https://gitlab.com/concatenation/nexus
```

- Then-- install the dependencies and start the server.

```
npm install
npm run start
```

# Usage

```lua
local Req = syn.request({
    Url = 'localhost:8080/createscript' -- url,
    Headers = {
        "Content-Type" = "application/json"
    },
    Body = game:GetService("HttpService").JSONEncode({
        scriptOwner = "scriptOwner",
        scriptLoadstring = "loadstring(...)",
        scriptName = "scriptName
    })
})
```
