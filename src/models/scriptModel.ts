import { Schema, model } from 'mongoose';
import { IScript } from '../utils/interfaces';

export const scriptsSchema: Schema<IScript> = new Schema<IScript>({
  scriptName: String,
  scriptLoadstring: String,
  scriptOwner: String || 'Anonymous'
})

export default model<IScript>('scripts-data', scriptsSchema)