import { Document } from 'mongoose';

export interface IScript extends Document {
  scriptName: string,
  scriptLoadstring?: string,
  scriptOwner: string,
}