import * as model from '../models/scriptModel';

function filter(script: string) {
  if (script.includes('loadstring')) {
    return true;
  }
  return false;
}

export async function createScript(scriptName: string, scriptLoadstring: string, scriptOwner?: string) {
  if (filter(scriptLoadstring)) {
    return await model.default.create({
      scriptName,
      scriptLoadstring,
      scriptOwner
    });
  }
}