import { createScript } from './src/utils/func';
import * as express from 'express';
import { connect } from 'mongoose';
import { config } from 'dotenv';

config({ path: __dirname + '/.env' });

connect(process.env.MONGODB_LOGIN as string, {
  autoReconnect: true,
  autoCreate: true,
  useFindAndModify: true,
  useNewUrlParser: true,
})
const app: express.Application = express.default();
app.use(express.json());

app.post('/createscript', async (req, res) => {
  if (req.body && req.body.scriptName && req.body.scriptLoadstring) {
    let scriptName = req.body.scriptName;
    let scriptLoadstring = req.body.scriptLoadstring;
    try {
      await createScript(scriptName, scriptLoadstring, req.body.scriptOwner || 'Anonymous').then(r => {
        res.status(200).json({
          error: false,
          data: JSON.stringify(r?.toJSON()) || 'Error'
        })
      })
    } catch (err) {
      if (err) {
        res.status(500).json({
          error: true,
          code: 500,
          text: '500 - Server Error',
          data: err
        })
      }
    }
  } else {
    res.status(406).json({
      error: true,
      code: 406,
      text: '406 - Not Acceptable',
      data: 'Invalid POST request!'
    })
  }
})